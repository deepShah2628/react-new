import React,{ Component } from 'react';
import Main from './components/MainComponent';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';//The <Provider> component makes the Redux store available to any nested components that need to access the Redux store.
import {ConfigureStore} from './redux/configureStore'

// By doing this store will be available
const store = ConfigureStore();
class App extends Component{

  render(){
  return (
    <Provider store={store} >
      <BrowserRouter>
        <div className="App">
          <Main />
        </div>
      </BrowserRouter>
    </Provider>
  );
  }
}


export default App;
