import { DISHES } from '../shared/dishes';
import { LEADERS } from '../shared/leaders';
import { PROMOTIONS } from '../shared/promotions';
import { COMMENTS } from '../shared/comments';

export const initialState={
    dishes: DISHES,
    comments:COMMENTS,
    promotions:PROMOTIONS,
    leaders:LEADERS
}

// As seen in the lecture we need a state and action for generating the next state using redux
// this is a pure function so the state should be not changed in any way
// here the state is uninitialized at the begining and if the state is uninitialized it will take the default value as initialState
export const Reducer=(state=initialState,action)=>{
    // we cannot modify the state directly here in the reduer . we can only give an immutable change and than return an updated version of the state fromm this reducer
    return state;
}