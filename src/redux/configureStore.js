// store as mentioned in the redux it holds the current state value
// Created using createStore()
// • Supplies three methods:
// – dispatch(): states state update with the provided action object
// – getState(): returns the current stored state value
// – subscribe(): accepts a callback function that will be run every
// time an action is dispatched
import { createStore } from 'redux';
import { Reducer , initialState } from './reducer';

export const ConfigureStore=()=>{
    const store = createStore(
        Reducer,//reducer
        initialState//Our initial state
    );
    return store;
}
