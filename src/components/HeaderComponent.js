import React,{Component} from 'react';
import { Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron,Button,Modal,ModalBody,ModalHeader, FormGroup, Label, Input,Form} from 'reactstrap';
import { NavLink } from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            isNavOpen: false,
            isModalOpen : false
        };
        this.toggleNav = this.toggleNav.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);

      }

      toggleNav() {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }
      toggleModal(){
       this.setState({
           isModalOpen: !this.state.isModalOpen
       });
    }
    handleLogin(event){
         this.toggleModal();
         alert("Username:"+this.username.value + " Password:" +this.password.value + " Remember"+this.remember.checked );
         event.preventDeafult();
    }
    render() {
        return(
            <div>
                <Navbar dark expand="md">   
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='Ristorante Con Fusion' /></NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/menu'><span className="fa fa-list fa-lg"></span> Menu</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/contactus'><span className="fa fa-address-card fa-lg"></span> Contact Us</NavLink>
                            </NavItem>
                            </Nav>
                            <Nav className ="ml-auto" navbar>
                                <NavItem >
                                    <Button outline onClick={this.toggleModal}>
                                        <span className="fa fa-sign-in fa-lg"></span>Login
                                    </Button>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className="row row-header">
                            <div className="col-12 col-sm-6">
                                <h1>Ristorante con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.handleLogin}>
                                <FormGroup>
                                    <Label htmlfor="username">UserName</Label>
                                    <Input type="text" id="username" name="username" placeholder="Enter UserName" 
                                    //We want to take the value to store the value in the username we will use ref and react use ref for some work so here we use inner ref
                                   innerRef={(input)=>this.username = input} />
                                </FormGroup>

                                <FormGroup>
                                    <Label htmlfor="password">Password</Label>
                                    <Input type="password" id="password" name="password" placeholder="Enter Password"
                                   innerRef={(input)=>this.password = input}
                                   />
                                </FormGroup>

                                <FormGroup check>
                                    <Label check>
                                    <Input type="checkbox" name="remember" innerRef={(input)=>this.remember = input}/>Keep Me Logged In 
                                    </Label>
                                </FormGroup>

                                <FormGroup>
                                   <Button type="submit" value="submit" className="bg-primary">Login</Button>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                    </Modal>
                </Jumbotron>
            </div>
        );
    }
}
export default Header;