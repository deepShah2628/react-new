import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Row, Col, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';

const required = (val) => val && val.length;
const maxLength = (len)=>(val) =>!(val) || (val.length<=len);
const minLength = (len)=>(val) => val && (val.length>=len);

export class ComponentForm extends Component{
    constructor(props){
        super(props);
        this.state={
           isModalOpen:false
        }
        this.toggleModal = this.toggleModal.bind(this);
    }
    handleSubmit(values) {
        this.toggleModal();
        alert("Current State is "+ JSON.stringify(values));
        
    }
    toggleModal(){
        this.setState({isModalOpen:!this.state.isModalOpen});
    }
    render(){
        return(
            <div>
                <Button outline onClick={this.toggleModal}>
                    <span className ="fa fa-pencil fa-lg">Submit Comment</span>
                </Button>
                <div className="row row-content">
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal} Submit comment>
                            <ModalBody>
                                <div className="col-xs-6">
                                    <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                                        <Row className="form-group">
                                            <Label htmlfor="rating" xs={12}>rating</Label>
                                            <Col xs={12}>
                                                <Control.select model=".rating" name="rating" className="form-control" >
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </Control.select>
                                            </Col>
                                        </Row>
                                        <Row className="form-group">
                                            <Label htmlfor="author" xs={12} >Your Name</Label>
                                            <Col xs={12}>
                                            <Control.text model=".author" name="author" className="form-control" placeholder="Your Name" 
                                            validators={{
                                                required,
                                                maxLength:maxLength(15),minLength:minLength(3)                                          
                                            }}
                                            />
                                            <Errors className="p-1 text-danger" model=".author" show="touched" messages={{ required: 'Required', minLength: 'Must be greater than 3 characters', maxLength: 'Must be 15 charaters or less' }} />
                                            </Col>
                                        </Row>
                                        <Row className="form-group">
                                            <Label htmlfor="feedback" xs={12} >Comments</Label>
                                            <Col xs={12}>
                                              <Control.textarea model=".comment" name="comment" className="form-control" rows="6" />
                                              </Col>
                                        </Row>
                                        <Button type="submit" value="submit" color="primary">Submit</Button>
                                    </LocalForm>

                                </div>
                            </ModalBody>
                        </ModalHeader>
                    </Modal>
                </div>
            </div>
        )
    }
}

function RenderDish({ dish }) {
    if (dish != null) {
        return (
            <div className="col-12 col-md-5 m-1">
                <Card>
                    <CardImg src={dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            </div>
        );
    }
    else {
        return (<div></div>)
    }
}

function RenderComments({ comments,postComment }) {
    if (comments != null) {
      
            return (
                <div className="col-12 col-md-5 m-1">
                    <h4>comments</h4>
                    <ul className="list-unstyled">
                      {comments.map((comment)=>{
                        return(
                        <li key={comment.id}>
                            <p>{comment.comment}</p>
                            <p>--{comment.author} , {new Intl.DateTimeFormat('en-us', { year: 'numeric', month: 'short', day: '2-digit' }).format
                                (new Date(Date.parse(comment.date)))}</p>
                        </li>
                        );
                      })}
                    </ul>
                    <ComponentForm  />
                </div>
            );
        

    }
    else {
        <div></div>
    }
}

const DishDetail = (props) => {
    if (props.dish != null) {
        return (
            <div className="container">
                 <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>
            </div>
                <div className="row">
                    <RenderDish dish={props.dish} />
                    <RenderComments comments={props.comments} />
                </div>
            </div>
        );
    }
    else {
        return (
            <div></div>
        )
    }
}

export default DishDetail;