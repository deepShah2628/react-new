import React from 'react';
import {Card, CardBody,CardImg,CardText,CardTitle,CardSubtitle} from 'reactstrap';

function RenderCard({item}){
  return(
    <Card>
      <CardImg src={item.image} alt ={item.name} />
      <CardBody>
        <CardTitle>{item.name}</CardTitle>

        {/* item designation is only for leaders.js so if it is true than return item.designation and if false return null. Due to JSX this kinds of things can be done(mixing html and js) */}
        {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle>:null}
        <CardText>{item.description}</CardText>
      </CardBody>
    </Card>
  )
}

function Home(props) {
    return(
      <div className="container">
        <div className="row align-items-start">
          <div className="col-12 col-md m-1">
            <RenderCard item={props.dish} />
          </div>
          <div className="col-12 col-md m-1">
            <RenderCard item={props.promotion} />
          </div>
          <div className="col-12 col-md m-1">
            <RenderCard item={props.leader} />
          </div>
        </div>
      </div>
    );
}

export default Home;   