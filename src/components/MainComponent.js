import React, { Component } from 'react';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import Contact from './ContactComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { Switch, Route, Redirect,withRouter } from 'react-router-dom';//withRouter is required to connect to redux
import { connect } from 'react-redux';//for connectiong this component to redux store
import DishDetail from './DishdetailComponent';
import About from './AboutComponent';

const mapStateToProps = state =>{
  return{
  dishes: state.dishes,
  comments:state.comments,
  promotions:state.promotions,
  leaders:state.leaders
}
}

class Main extends Component {

  constructor(props) {
    super(props);
   
  }
  
 
  render() {

      const HomePage = ()=>{
        return( 
          // for all dishes whose dish filterd is true will be returned and if the data is arranged properly than only one dish item filtered(property given to dishes.js) would be true.Note the filter property returns an array
        <Home dish={this.props.dishes.filter((dish)=>dish.featured)[0]}
        promotion ={this.props.promotions.filter((promotion)=>promotion.featured)[0]}
        leader = {this.props.leaders.filter((leader)=>leader.featured)[0]}
        />
        );
      }

      const DishWithId=({match})=>{
      //  The parse int will convert the data returned by the match.params.dishId (dish.id mapped) to radix base 10
        return(
          <DishDetail dish={this.props.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]} 
          comments={this.props.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))} />
        );
      }
     
    return (
      <div>
        <div className="container">
          <Header />
          <Switch>
            <Route path="/home" component={HomePage} />
             
             {/* By directly specifing like {HomePage] we cannot pass any content as props so if we want to pass the content as props than we may require to use arrow function */}
            <Route exact path="/menu" component={()=><Menu dishes={this.props.dishes} />} />
            <Route path='/menu/:dishId' component={DishWithId} />
            <Route exact path="/contactus" component={Contact} />
            <Route exact path="/aboutus" component={()=><About leaders={this.props.leaders}/>}/>
            <Redirect to='/Home' />
          </Switch>
          <Footer />
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Main));//As we are using react router in ordder to make things work we used react rouuter
